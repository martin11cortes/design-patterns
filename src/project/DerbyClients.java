/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author usuario
 */
public class DerbyClients implements DbConnection {

    private static DerbyClients instance;
    private Connection conn = null;

    public static DerbyClients getInstance() {
        if (instance == null) {
            instance = new DerbyClients();
        }
        return instance;
    }

    @Override
    public Connection getConnection() throws SQLException {

        if (conn == null || conn.isClosed()) {
            try {
                String dbUrl = "jdbc:derby:memory:codejava/webdb;create=true";
                conn = DriverManager.getConnection(dbUrl);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return conn;
    }

}
