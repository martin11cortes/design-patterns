/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project;

import java.util.ArrayList;

/**
 *
 * @author usuario
 */
public class Project {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        FacadeDatabase db = new FacadeDatabase();

        ArrayList listaAsesores = db.selectUsers();
        ArrayList listaClientes = db.selectClients();

        Campana myCampana = new Campana.Builder()
                .asesores(listaAsesores)
                .canal("mensajes")
                .clientes(listaClientes)
                .distribucion(new DistribucionPorAntiguedad())
                .crear();
        myCampana.getClientes();

    }

}
