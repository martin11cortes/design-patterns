/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project;

import java.util.ArrayList;

/**
 *
 * @author usuario
 */
public class Campana {

    private final String canal;
    private final ArrayList asesores;
    private final ArrayList clientes;
    private final DistribucionClientes strategy;

    private Campana(Builder builder) {
        this.canal = builder.canal;
        this.asesores = builder.asesores;
        this.clientes = builder.clientes;
        this.strategy = builder.strategy;
    }

    public static class Builder {

        private String canal;
        private ArrayList asesores;
        private ArrayList clientes;
        private DistribucionClientes strategy;

        public Builder canal(String canal) {
            this.canal = canal;
            return this;
        }

        public Builder asesores(ArrayList asesores) {
            this.asesores = asesores;
            return this;
        }

        public Builder clientes(ArrayList clientes) {
            this.clientes = clientes;
            return this;
        }

        public Builder distribucion(DistribucionClientes strategy) {
            this.strategy = strategy;
            return this;
        }

        public Campana crear() {
            return new Campana(this);
        }
    }

    /**
     * @return the canal
     */
    public String getCanal() {
        return canal;
    }

    /**
     * @return the asesores
     */
    public ArrayList getAsesores() {
        return asesores;
    }

    /**
     * @return the client's
     */
    public ArrayList getClientes() {
        return clientes;
    }

    /**
     * @return the strategy
     */
    public DistribucionClientes getStrategy() {
        return strategy;
    }

}
