/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usuario
 */
public class FacadeDatabase {

    private Connection users;
    private Connection clients;

    public FacadeDatabase() {

        try {
            users = new DatabaseFactory().getDatabase("USERS").getConnection();
            clients = new DatabaseFactory().getDatabase("CLIENTS").getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(FacadeDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public ArrayList selectUsers() {

        ArrayList usersList = new ArrayList<>();
        ResultSet rs;
        try {
            rs = users.createStatement().executeQuery("SELECT * FROM users");
            while (rs.next()) {
                usersList.add(rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3));
            }
        } catch (SQLException ex) {
            Logger.getLogger(FacadeDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return usersList;
    }

    public ArrayList selectClients() {
        ArrayList clientList = new ArrayList<>();
        ResultSet rs;
        try {
            rs = clients.createStatement().executeQuery("SELECT * FROM clients");
            while (rs.next()) {
                clientList.add(rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3));
            }
        } catch (SQLException ex) {
            Logger.getLogger(FacadeDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return clientList;
    }
    
    public ArrayList clientsByUsers(){
        ArrayList clientsByUserList = new ArrayList<>();
        //buscar usuarios en la conexion correspondiente
        //recorrer usuarios y dentro buscar los clientes de cada uno 
        //asignar el resultado a un arreglo de clientes por usuario
        return clientsByUserList;
    }
}
