/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author usuario
 */
public interface DbConnection {

    public Connection getConnection() throws SQLException;

}
