/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project;

/**
 *
 * @author usuario
 */
public class Asesor {

    private int codigo;
    private String nombre;
    private String experiencia;
    private int horasPorDia;
    private int diasPorSemana;

    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the experiencia
     */
    public String getExperiencia() {
        return experiencia;
    }

    /**
     * @param experiencia the experiencia to set
     */
    public void setExperiencia(String experiencia) {
        this.experiencia = experiencia;
    }

    /**
     * @return the horasPorDia
     */
    public int getHorasPorDia() {
        return horasPorDia;
    }

    /**
     * @param horasPorDia the horasPorDia to set
     */
    public void setHorasPorDia(int horasPorDia) {
        this.horasPorDia = horasPorDia;
    }

    /**
     * @return the diasPorSemana
     */
    public int getDiasPorSemana() {
        return diasPorSemana;
    }

    /**
     * @param diasPorSemana the diasPorSemana to set
     */
    public void setDiasPorSemana(int diasPorSemana) {
        this.diasPorSemana = diasPorSemana;
    }
}
