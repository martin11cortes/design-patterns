/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project;

/**
 *
 * @author usuario
 */
public class DatabaseFactory implements ConnectionFactory {

    @Override
    public DbConnection getDatabase(String Schema) {
        // Crear por version de la base de datos o por esquema
        // Como usar ENUM's
        switch (Schema) {
            case "USERS":
                return DerbyClients.getInstance();
            case "CLIENTS":
                return MySqlUsers.getInstance();
            default:
                throw new IllegalArgumentException("Schema not found");
        }
    }

}
